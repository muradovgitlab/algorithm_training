if __name__ == '__main__':
    # str = "Цветом мой зайчишка – белый,\nА ещё, он очень смелый!\nНе боится он лисицы,\nЛьва он тоже не боится."
    str = "Muradov Kamil Dzhavidovich"
    print ("String:", str)
    alphabet = dict.fromkeys(list(str), 0)
    sum = 0
    for i in alphabet:
            alphabet[i] = str.count(i)
            sum += str.count(i)
    alphabet["Space"] = alphabet.pop(" ")
    # alphabet["EOL symbol"] = alphabet.pop("\n")
    alphabet = dict(sorted(alphabet.items(), key = lambda item: item[1], reverse = True))
    print("\n".join("key: {!r} value: {!r} prob: {!r}".format(k, v, sum/100*v) for k, v in alphabet.items()))
    mid_val = 0
    disp = 0
    print ("\n")
    for k, v in alphabet.items():
        if (v == 3):
            print ("for:", k, "sr = ", 3, "*", sum/100*v)
            mid_val = 3*sum/100*v
        elif (v == 2):
            print ("for:", k, "sr = ", 4, "*", sum/100*v)
            mid_val = 4*sum/100*v
        elif (v == 1):
            print ("for:", k, "sr = ", 5, "*", sum/100*v)
            mid_val = 5*sum/100*v
    print ("\nmid val:", mid_val)
    for k, v in alphabet.items():
        if (v == 3):
            disp = sum/100*v*(3 - mid_val)
        elif (v == 2):
            disp = sum/100*v*(4 - mid_val)
        elif (v == 1):
            disp = sum/100*v*(5 - mid_val)
    print ("dispersion:", disp)
    # huffman_code(alphabet.keys())s